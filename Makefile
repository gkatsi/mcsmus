###################################################################################################

.PHONY:	r d p sh cr cd cp csh lr ld lp lsh config all install install-headers install-lib\
        install-bin clean distclean
all:	r

## Load Previous Configuration ####################################################################

-include config.mk

## Configurable options ###########################################################################

# Directory to store object files, libraries, executables, and dependencies:
BUILD_DIR      ?= build

# Include debug-symbols in release builds
MCSMUS_RELSYM ?= -g

# Sets of compile flags for different build types
MCSMUS_REL    ?= -O3 #-D NDEBUG
MCSMUS_DEB    ?= -O0 -D DEBUG
MCSMUS_PRF    ?= -O3 -D NDEBUG
MCSMUS_FPIC   ?= -fpic

# GNU Standard Install Prefix
prefix         ?= /usr/local

ifneq ($(LGL_DIR), '')
LGL_CXXFLAGS=-DUSE_LINGELING -I$(LGL_DIR)
LGL_LDFLAGS=$(LGL_DIR)/liblgl.a
else
LGL_CXXFLAGS=
LGL_LDFLAGS=
endif

## Write Configuration  ###########################################################################

config:
	@( echo 'BUILD_DIR?=$(BUILD_DIR)'         ; \
	   echo 'MCSMUS_RELSYM?=$(MCSMUS_RELSYM)' ; \
	   echo 'MCSMUS_REL?=$(MCSMUS_REL)'       ; \
	   echo 'MCSMUS_DEB?=$(MCSMUS_DEB)'       ; \
	   echo 'MCSMUS_PRF?=$(MCSMUS_PRF)'       ; \
	   echo 'MCSMUS_FPIC?=$(MCSMUS_FPIC)'     ; \
	   echo 'LGL_DIR?=$(LGL_DIR)'             ; \
	   echo 'AR?=$(AR)'                       ; \
	   echo 'LTO?=$(LTO)'                     ; \
	   echo 'prefix?=$(prefix)'                 ) > config.mk

## Configurable options end #######################################################################

INSTALL ?= install

# GNU Standard Install Variables
exec_prefix ?= $(prefix)
includedir  ?= $(prefix)/include
bindir      ?= $(exec_prefix)/bin
libdir      ?= $(exec_prefix)/lib
datarootdir ?= $(prefix)/share
mandir      ?= $(datarootdir)/man

# Target file names
MCSMUS      = mcsmus#       Name of Mcsmus main executable.
MCSMUS_SLIB = lib$(MCSMUS).a#  Name of Mcsmus static library.
MCSMUS_DLIB = lib$(MCSMUS).so# Name of Mcsmus shared library.

# Shared Library Version
SOMAJOR=1
SOMINOR=0
SORELEASE?=.0#   Declare empty to leave out from library file name.

MCSMUS_CXXFLAGS = -I. -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS -Wall -Wno-parentheses -Wextra -std=c++17 $(LGL_CXXFLAGS)
MCSMUS_LDFLAGS  = -Wall -lz $(LGL_LDFLAGS)

ECHO=@echo
ifeq ($(VERB),)
VERB=@
else
VERB=
endif

SRCS = $(wildcard minisat/core/*.cc) $(wildcard minisat/simp/*.cc) $(wildcard minisat/utils/*.cc) \
	$(wildcard glucose/core/*.cc) $(wildcard glucose/simp/*.cc) $(wildcard glucose/utils/*.cc) \
	$(wildcard mcsmus/*.cc)
HDRS = $(wildcard minisat/mtl/*.h) $(wildcard minisat/core/*.h) $(wildcard minisat/simp/*.h) $(wildcard minisat/utils/*.h) \
	$(wildcard glucose/mtl/*.h) $(wildcard glucose/core/*.h) $(wildcard glucose/simp/*.h) $(wildcard glucose/utils/*.h) \
	$(wildcard mcsmus/*.hh)
OBJS = $(filter-out %Main.o, $(SRCS:.cc=.o))

r:	$(BUILD_DIR)/release/bin/$(MCSMUS)
d:	$(BUILD_DIR)/debug/bin/$(MCSMUS)
p:	$(BUILD_DIR)/profile/bin/$(MCSMUS)
sh:	$(BUILD_DIR)/dynamic/bin/$(MCSMUS)

lr:	$(BUILD_DIR)/release/lib/$(MCSMUS_SLIB)
ld:	$(BUILD_DIR)/debug/lib/$(MCSMUS_SLIB)
lp:	$(BUILD_DIR)/profile/lib/$(MCSMUS_SLIB)
lsh:	$(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE)

## Build-type Compile-flags:
$(BUILD_DIR)/release/%.o:			MCSMUS_CXXFLAGS +=$(MCSMUS_REL) $(MCSMUS_RELSYM) $(LTO)
$(BUILD_DIR)/debug/%.o:				MCSMUS_CXXFLAGS +=$(MCSMUS_DEB) -g -D_GLIBCXX_DEBUG
$(BUILD_DIR)/profile/%.o:			MCSMUS_CXXFLAGS +=$(MCSMUS_PRF) -pg
$(BUILD_DIR)/dynamic/%.o:			MCSMUS_CXXFLAGS +=$(MCSMUS_REL) $(MCSMUS_FPIC)

## Build-type Link-flags:
$(BUILD_DIR)/profile/bin/$(MCSMUS):		MCSMUS_LDFLAGS += -pg
$(BUILD_DIR)/release/bin/$(MCSMUS):		MCSMUS_LDFLAGS += --static $(MCSMUS_RELSYM) $(LTO)

## Executable dependencies
$(BUILD_DIR)/release/bin/$(MCSMUS):	 	$(BUILD_DIR)/release/mcsmus/Main.o $(BUILD_DIR)/release/lib/$(MCSMUS_SLIB)
$(BUILD_DIR)/debug/bin/$(MCSMUS):	 	        $(BUILD_DIR)/debug/mcsmus/Main.o $(BUILD_DIR)/debug/lib/$(MCSMUS_SLIB)
$(BUILD_DIR)/profile/bin/$(MCSMUS):	 	$(BUILD_DIR)/profile/mcsmus/Main.o $(BUILD_DIR)/profile/lib/$(MCSMUS_SLIB)
# need the main-file be compiled with fpic?
$(BUILD_DIR)/dynamic/bin/$(MCSMUS):	 	$(BUILD_DIR)/dynamic/mcsmus/Main.o $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB)

## Library dependencies
$(BUILD_DIR)/release/lib/$(MCSMUS_SLIB):	$(foreach o,$(OBJS),$(BUILD_DIR)/release/$(o))
$(BUILD_DIR)/debug/lib/$(MCSMUS_SLIB):		$(foreach o,$(OBJS),$(BUILD_DIR)/debug/$(o))
$(BUILD_DIR)/profile/lib/$(MCSMUS_SLIB):	$(foreach o,$(OBJS),$(BUILD_DIR)/profile/$(o))
$(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE)\
 $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR)\
 $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB):	$(foreach o,$(OBJS),$(BUILD_DIR)/dynamic/$(o))

## Compile rules (these should be unified, buit I have not yet found a way which works in GNU Make)
$(BUILD_DIR)/release/%.o:	%.cc
	$(ECHO) Compiling: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $(MCSMUS_CXXFLAGS) $(CXXFLAGS) -c -o $@ $< -MMD -MF $(BUILD_DIR)/release/$*.d

$(BUILD_DIR)/profile/%.o:	%.cc
	$(ECHO) Compiling: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $(MCSMUS_CXXFLAGS) $(CXXFLAGS) -c -o $@ $< -MMD -MF $(BUILD_DIR)/profile/$*.d

$(BUILD_DIR)/debug/%.o:	%.cc
	$(ECHO) Compiling: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $(MCSMUS_CXXFLAGS) $(CXXFLAGS) -c -o $@ $< -MMD -MF $(BUILD_DIR)/debug/$*.d

$(BUILD_DIR)/dynamic/%.o:	%.cc
	$(ECHO) Compiling: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $(MCSMUS_CXXFLAGS) $(CXXFLAGS) -c -o $@ $< -MMD -MF $(BUILD_DIR)/dynamic/$*.d

## Linking rule
$(BUILD_DIR)/release/bin/$(MCSMUS) $(BUILD_DIR)/debug/bin/$(MCSMUS) $(BUILD_DIR)/profile/bin/$(MCSMUS) $(BUILD_DIR)/dynamic/bin/$(MCSMUS):
	$(ECHO) Linking Binary: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $^ $(MCSMUS_LDFLAGS) $(LDFLAGS) -o $@

## Static Library rule
%/lib/$(MCSMUS_SLIB):
	$(ECHO) Linking Static Library: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(AR) -rcs $@ $^

## Shared Library rule
$(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE)\
 $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR)\
 $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB):
	$(ECHO) Linking Shared Library: $@
	$(VERB) mkdir -p $(dir $@)
	$(VERB) $(CXX) $(MCSMUS_LDFLAGS) $(LDFLAGS) -o $@ -shared -Wl,-soname,$(MCSMUS_DLIB).$(SOMAJOR) $^
	$(VERB) ln -sf $(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE) $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR)
	$(VERB) ln -sf $(MCSMUS_DLIB).$(SOMAJOR) $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB)

install:	install-headers install-lib install-bin
install-debug:	install-headers install-lib-debug

install-headers:
#       Create directories
	$(INSTALL) -d $(DESTDIR)$(includedir)/mcsmus
	for dir in minisat/mtl minisat/utils minisat/core minisat/simp \
	          glucose/core glucose/simp; do \
	  $(INSTALL) -d $(DESTDIR)$(includedir)/$$dir ; \
	done
#       Install headers
	for h in $(HDRS) ; do \
	  $(INSTALL) -m 644 $$h $(DESTDIR)$(includedir)/$$h ; \
	done

install-lib-debug: $(BUILD_DIR)/debug/lib/$(MCSMUS_SLIB)
	$(INSTALL) -d $(DESTDIR)$(libdir)
	$(INSTALL) -m 644 $(BUILD_DIR)/debug/lib/$(MCSMUS_SLIB) $(DESTDIR)$(libdir)

install-lib: $(BUILD_DIR)/release/lib/$(MCSMUS_SLIB) $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE)
	$(INSTALL) -d $(DESTDIR)$(libdir)
	$(INSTALL) -m 644 $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE) $(DESTDIR)$(libdir)
	ln -sf $(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE) $(DESTDIR)$(libdir)/$(MCSMUS_DLIB).$(SOMAJOR)
	ln -sf $(MCSMUS_DLIB).$(SOMAJOR) $(DESTDIR)$(libdir)/$(MCSMUS_DLIB)
	$(INSTALL) -m 644 $(BUILD_DIR)/release/lib/$(MCSMUS_SLIB) $(DESTDIR)$(libdir)

install-bin: $(BUILD_DIR)/dynamic/bin/$(MCSMUS)
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL) -m 755 $(BUILD_DIR)/dynamic/bin/$(MCSMUS) $(DESTDIR)$(bindir)

clean:
	rm -f $(foreach t, release debug profile dynamic, $(foreach o, $(SRCS:.cc=.o), $(BUILD_DIR)/$t/$o)) \
          $(foreach t, release debug profile dynamic, $(foreach d, $(SRCS:.cc=.d), $(BUILD_DIR)/$t/$d)) \
	  $(foreach t, release debug profile dynamic, $(BUILD_DIR)/$t/bin/$(MCSMUS_CORE) $(BUILD_DIR)/$t/bin/$(MCSMUS)) \
	  $(foreach t, release debug profile, $(BUILD_DIR)/$t/lib/$(MCSMUS_SLIB)) \
	  $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR).$(SOMINOR)$(SORELEASE)\
	  $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB).$(SOMAJOR)\
	  $(BUILD_DIR)/dynamic/lib/$(MCSMUS_DLIB)

distclean:	clean
	rm -f config.mk

## Include generated dependencies
-include $(foreach s, $(SRCS:.cc=.d), $(BUILD_DIR)/release/$s)
-include $(foreach s, $(SRCS:.cc=.d), $(BUILD_DIR)/debug/$s)
-include $(foreach s, $(SRCS:.cc=.d), $(BUILD_DIR)/profile/$s)
-include $(foreach s, $(SRCS:.cc=.d), $(BUILD_DIR)/dynamic/$s)
